package com.epam.task18.jdbc;

import com.epam.task18.jdbc.view.ConsoleView;

public class Application {

  public static void main(String[] args) {
    new ConsoleView().run();
  }
}
