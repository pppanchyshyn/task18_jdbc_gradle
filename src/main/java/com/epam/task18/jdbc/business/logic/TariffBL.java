package com.epam.task18.jdbc.business.logic;

import com.epam.task18.jdbc.dao.implementation.TariffDaoImp;
import com.epam.task18.jdbc.model.Tariff;
import java.sql.SQLException;
import java.util.List;

public class TariffBL {

  public TariffBL() {
  }

  public List<Tariff> getAll() {
    List<Tariff> tariffs = null;
    try {
      tariffs = new TariffDaoImp().getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return tariffs;
  }

  public Tariff getByKey(Integer key) {
    Tariff tariff = null;
    try {
      tariff = new TariffDaoImp().getByKey(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return tariff;
  }

  public int create(Tariff entity) {
    int result = 0;
    try {
      result = new TariffDaoImp().create(entity);
    } catch (SQLException e) {
      e.getErrorCode();
    }
    return result;
  }

  public int update(Tariff entity) {
    int result = 0;
    try {
      result = new TariffDaoImp().update(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int delete(Integer key) {
    int result = 0;
    try {
      result = new TariffDaoImp().delete(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }
}
