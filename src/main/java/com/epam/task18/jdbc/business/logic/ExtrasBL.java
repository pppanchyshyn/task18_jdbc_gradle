package com.epam.task18.jdbc.business.logic;

import com.epam.task18.jdbc.dao.implementation.ExtrasDaoImp;
import com.epam.task18.jdbc.model.Extras;
import java.sql.SQLException;
import java.util.List;

public class ExtrasBL {

  public ExtrasBL() {
  }

  public List<Extras> getAll() {
    List<Extras> extrasList = null;
    try {
      extrasList =  new ExtrasDaoImp().getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return extrasList;
  }

  public Extras getByKey(Integer key) {
    Extras extras = null;
    try {
      extras = new ExtrasDaoImp().getByKey(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return extras;
  }

  public int create(Extras entity) {
    int result = 0;
    try {
      result =  new ExtrasDaoImp().create(entity);
    } catch (SQLException e) {
      e.getErrorCode();
    }
    return result;
  }

  public int update(Extras entity) {
    int result = 0;
    try {
      result = new ExtrasDaoImp().update(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int delete(Integer key) {
    int result = 0;
    try {
      result = new ExtrasDaoImp().delete(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }
}
