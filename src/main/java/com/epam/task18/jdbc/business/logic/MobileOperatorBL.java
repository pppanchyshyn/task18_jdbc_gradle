package com.epam.task18.jdbc.business.logic;

import com.epam.task18.jdbc.dao.implementation.MobileOperatorDaoImp;
import com.epam.task18.jdbc.model.MobileOperator;
import java.sql.SQLException;
import java.util.List;

public class MobileOperatorBL {

  public MobileOperatorBL() {
  }

  public List<MobileOperator> getAll() {
    List<MobileOperator> mobileOperators = null;
    try {
      mobileOperators = new MobileOperatorDaoImp().getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return mobileOperators;
  }

  public MobileOperator getByKey(Integer key) {
    MobileOperator mobileOperator = null;
    try {
      mobileOperator = new MobileOperatorDaoImp().getByKey(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return mobileOperator;
  }

  public int create(MobileOperator entity) {
    int result = 0;
    try {
      result = new MobileOperatorDaoImp().create(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int update(MobileOperator entity) {
    int result = 0;
    try {
      result = new MobileOperatorDaoImp().update(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int delete(Integer key) {
    int result = 0;
    try {
      result = new MobileOperatorDaoImp().delete(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }
}
