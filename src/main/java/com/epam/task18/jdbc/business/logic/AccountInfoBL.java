package com.epam.task18.jdbc.business.logic;

import com.epam.task18.jdbc.dao.implementation.AccountInfoDaoImp;
import com.epam.task18.jdbc.model.AccountInfo;
import java.sql.SQLException;
import java.util.List;

public class AccountInfoBL {

  public AccountInfoBL() {
  }

  public List<AccountInfo> getAll() {
    List<AccountInfo> accountInfoList = null;
    try {
      accountInfoList = new AccountInfoDaoImp().getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return accountInfoList;
  }

  public AccountInfo getByKey(Integer key) {
    AccountInfo accountInfo = null;
    try {
      accountInfo =  new AccountInfoDaoImp().getByKey(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return accountInfo;
  }

  public int create(AccountInfo entity) {
    int result = 0;
    try {
      result =  new AccountInfoDaoImp().create(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int update(AccountInfo entity) {
    int result = 0;
    try {
      result = new AccountInfoDaoImp().update(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int delete(Integer key) {
    int result = 0;
    try {
      return new AccountInfoDaoImp().delete(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }
}
