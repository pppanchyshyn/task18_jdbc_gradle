package com.epam.task18.jdbc.view;

import com.epam.task18.jdbc.controller.Controller;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView {

  private Map<String, String> mainMenu;
  private Controller controller;
  public static Logger logger = LogManager.getLogger(ConsoleView.class);
  private Scanner input = new Scanner(System.in);

  public ConsoleView() {
    controller = new Controller();
    mainMenu = new LinkedHashMap<>();
    mainMenu.put("A", "A - Print all tables");
    mainMenu.put("1", "1 - Table Mobile Operator");
    mainMenu.put("11", "11 - Print table Mobile Operator");
    mainMenu.put("12", "12 - Get Mobile Operator by id");
    mainMenu.put("13", "13 - Create new member in Mobile Operator");
    mainMenu.put("14", "14 - Update member in Mobile Operator");
    mainMenu.put("15", "15 - Delete member in Mobile Operator by id");
    mainMenu.put("2", "2 - Table Tariff");
    mainMenu.put("21", "21 - Print table Tariff");
    mainMenu.put("22", "22 - Get Tariff by id");
    mainMenu.put("23", "23 - Create new member in Tariff");
    mainMenu.put("24", "24 - Update member in Tariff");
    mainMenu.put("25", "25 - Delete member in Tariff by id");
    mainMenu.put("3", "3 - Table Service Centre");
    mainMenu.put("31", "31 - Print table Service Centre");
    mainMenu.put("32", "32 - Get Service Centre by id");
    mainMenu.put("33", "33 - Create new member in Service Centre");
    mainMenu.put("34", "34 - Update member in Service Centre");
    mainMenu.put("35", "35 - Delete member in Service Centre by id");
    mainMenu.put("4", "4 - Table Extras");
    mainMenu.put("41", "41 - Print table Extras");
    mainMenu.put("42", "42 - Get Extras by id");
    mainMenu.put("43", "43 - Create new member in Extras");
    mainMenu.put("44", "44 - Update member in Extras");
    mainMenu.put("45", "45 - Delete member in Extras by id");
    mainMenu.put("5", "5 - Table Tariff Terms");
    mainMenu.put("51", "51 - Print table Tariff Terms");
    mainMenu.put("52", "52 - Get Tariff Terms by id");
    mainMenu.put("53", "53 - Create new member in Tariff Terms");
    mainMenu.put("54", "54 - Update member in Tariff Terms");
    mainMenu.put("55", "55 - Delete member in Tariff Terms by id");
    mainMenu.put("6", "6 - Table Client");
    mainMenu.put("61", "61 - Print table Client");
    mainMenu.put("62", "62 - Get Client by id");
    mainMenu.put("63", "63 - Get clients by Mobile Operator id");
    mainMenu.put("64", "64 - Create new member in Client");
    mainMenu.put("65", "65 - Update member in Client");
    mainMenu.put("66", "66 - Delete member in Client by id");
    mainMenu.put("7", "7 - Table Account Info");
    mainMenu.put("71", "71 - Print table Account Info");
    mainMenu.put("72", "72 - Get Account Info by client id");
    mainMenu.put("73", "73 - Create new member in Account Info");
    mainMenu.put("74", "74 - Update member in Account Info");
    mainMenu.put("75", "75 - Delete member in Account Info by client id");
    mainMenu.put("8", "8 - Table Client Extras");
    mainMenu.put("81", "81 - Print table Client Extras");
    mainMenu.put("82", "82 - Get Client Extras by client id");
    mainMenu.put("83", "83 - Get Client Extras by extras id");
    mainMenu.put("84", "84 - Create new member in Client Extras");
    mainMenu.put("85", "85 - Delete member in Client Extras");
    mainMenu.put("E", "E - Exit");
  }

  private void printMainMenu() {
    logger.info(String.format("%15s", "Menu:"));
    for (String item : mainMenu.keySet()) {
      if (item.length() == 1) {
        logger.info(mainMenu.get(item));
      }
    }
  }

  private void printSubMenu(String subItem) {
    logger.info(String.format("%15s", "SubMenu:"));
    for (String item : mainMenu.keySet()) {
      if (item.length() != 1 && item.substring(0, 1).equals(subItem)) {
        logger.info(mainMenu.get(item));
      }
    }
  }

  public void run() {
    String menuItem = "";
    while (!menuItem.equals("E")) {
      printMainMenu();
      logger.info("Select menu item :");
      menuItem = input.nextLine().toUpperCase();
      if (menuItem.matches("^\\d")) {
        printSubMenu(menuItem);
        logger.info("Select menu item :");
        menuItem = input.nextLine().toUpperCase();
      }
      controller.getFunctionalMenu().get(menuItem).print();
    }
  }
}
