package com.epam.task18.jdbc.dao.interfaces;

import com.epam.task18.jdbc.model.TariffTerms;

public interface TariffTermsDao extends Dao<TariffTerms, Integer> {

}
