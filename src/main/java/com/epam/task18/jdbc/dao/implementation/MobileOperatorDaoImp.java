package com.epam.task18.jdbc.dao.implementation;

import com.epam.task18.jdbc.dao.interfaces.MobileOperatorDao;
import com.epam.task18.jdbc.model.MobileOperator;
import com.epam.task18.jdbc.utilities.JdbcConnector;
import com.epam.task18.jdbc.utilities.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MobileOperatorDaoImp implements MobileOperatorDao {

  private static final String GET_ALL = "SELECT * FROM mobile_operator";
  private static final String GET_BY_KEY = "SELECT * FROM mobile_operator WHERE id=?";
  private static final String CREATE = "INSERT mobile_operator (id, name) VALUES (?, ?)";
  private static final String UPDATE = "UPDATE mobile_operator SET name=? WHERE id=?";
  private static final String DELETE = "DELETE FROM mobile_operator WHERE id=?";

  @Override
  public List<MobileOperator> getAll() throws SQLException {
    List<MobileOperator> operators = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement rs = connection.prepareStatement(GET_ALL)) {
      try (ResultSet resultSet = rs.executeQuery()) {
        while (resultSet.next()) {
          operators.add((MobileOperator) new Transformer(MobileOperator.class)
              .transform(resultSet));
        }
      }
    }
    return operators;
  }

  @Override
  public MobileOperator getByKey(Integer key) throws SQLException {
    MobileOperator entity = null;
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_KEY)) {
      ps.setInt(1, key);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity = (MobileOperator) new Transformer(MobileOperator.class)
              .transform(resultSet);
          break;
        }
      }
    }
    return entity;
  }

  @Override
  public int create(MobileOperator entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getId());
      ps.setString(2, entity.getName());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(MobileOperator entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
      ps.setString(1, entity.getName());
      ps.setInt(2, entity.getId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(Integer key) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
      ps.setInt(1, key);
      return ps.executeUpdate();
    }
  }
}
