package com.epam.task18.jdbc.dao.interfaces;

import com.epam.task18.jdbc.model.MobileOperator;

public interface MobileOperatorDao extends Dao<MobileOperator, Integer>{

}
