package com.epam.task18.jdbc.dao.interfaces;

import com.epam.task18.jdbc.model.Tariff;

public interface TariffDao extends Dao<Tariff, Integer> {

}
