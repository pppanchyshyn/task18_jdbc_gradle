package com.epam.task18.jdbc.dao.interfaces;

import java.sql.SQLException;
import java.util.List;

public interface Dao<T, K> {

  int create(T entity) throws SQLException;

  int update(T entity) throws SQLException;

  int delete(K key) throws SQLException;

  T getByKey(K key) throws SQLException;

  List<T> getAll() throws SQLException;
}
