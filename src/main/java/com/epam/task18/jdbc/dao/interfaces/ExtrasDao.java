package com.epam.task18.jdbc.dao.interfaces;

import com.epam.task18.jdbc.model.Extras;

public interface ExtrasDao extends Dao<Extras, Integer> {

}
