package com.epam.task18.jdbc.dao.implementation;

import com.epam.task18.jdbc.dao.interfaces.ExtrasDao;
import com.epam.task18.jdbc.model.Extras;
import com.epam.task18.jdbc.utilities.JdbcConnector;
import com.epam.task18.jdbc.utilities.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExtrasDaoImp implements ExtrasDao {
  private static final String GET_ALL = "SELECT * FROM extras";
  private static final String GET_BY_KEY = "SELECT * FROM extras WHERE id=?";
  private static final String CREATE = "INSERT extras (id, name, description, price, operator_id) VALUES (?, ?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE extras SET name=?, description=?, price=?, operator_id=? WHERE id=?";
  private static final String DELETE = "DELETE FROM extras WHERE id=?";
  @Override
  public List<Extras> getAll() throws SQLException {
    List<Extras> extrasList = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          extrasList.add((Extras) new Transformer(Extras.class)
              .transform(resultSet));
        }
      }
    }
    return extrasList;
  }

  @Override
  public Extras getByKey(Integer key) throws SQLException {
    Extras entity = null;
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_KEY)) {
      ps.setInt(1, key);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity = (Extras) new Transformer(Extras.class)
              .transform(resultSet);
          break;
        }
      }
    }
    return entity;
  }

  @Override
  public int create(Extras entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getId());
      ps.setString(2, entity.getName());
      ps.setString(3, entity.getDescription());
      ps.setInt(4, entity.getPrice());
      ps.setInt(5, entity.getOperatorId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(Extras entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
      ps.setString(1, entity.getName());
      ps.setString(2, entity.getDescription());
      ps.setInt(3, entity.getPrice());
      ps.setInt(4, entity.getOperatorId());
      ps.setInt(5, entity.getId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(Integer key) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
      ps.setInt(1, key);
      return ps.executeUpdate();
    }
  }
}
