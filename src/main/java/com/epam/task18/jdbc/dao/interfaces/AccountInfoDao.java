package com.epam.task18.jdbc.dao.interfaces;

import com.epam.task18.jdbc.model.AccountInfo;

public interface AccountInfoDao extends Dao<AccountInfo, Integer> {

}
