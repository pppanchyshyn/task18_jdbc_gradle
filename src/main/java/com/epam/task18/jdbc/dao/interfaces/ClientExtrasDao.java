package com.epam.task18.jdbc.dao.interfaces;

import com.epam.task18.jdbc.model.ClientExtras;
import com.epam.task18.jdbc.model.ClientExtrasPrimaryKey;
import java.sql.SQLException;
import java.util.List;

public interface ClientExtrasDao extends Dao<ClientExtras, ClientExtrasPrimaryKey> {
  List<ClientExtras> getByExtrasId(Integer extrasId) throws SQLException;
  List<ClientExtras> getByClientId(Integer clientId) throws SQLException;
}
