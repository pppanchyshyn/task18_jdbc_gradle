package com.epam.task18.jdbc.dao.implementation;

import com.epam.task18.jdbc.dao.interfaces.TariffDao;
import com.epam.task18.jdbc.model.Tariff;
import com.epam.task18.jdbc.utilities.JdbcConnector;
import com.epam.task18.jdbc.utilities.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TariffDaoImp implements TariffDao {

  private static final String GET_ALL = "SELECT * FROM tariff";
  private static final String GET_BY_KEY = "SELECT * FROM tariff WHERE id=?";
  private static final String CREATE = "INSERT tariff (id, name, fee, operator_id) VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE tariff SET name=?, fee=?, operator_id=? WHERE id=?";
  private static final String DELETE = "DELETE FROM tariff WHERE id=?";

  @Override
  public List<Tariff> getAll() throws SQLException {
    List<Tariff> tariffs = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          tariffs.add((Tariff) new Transformer(Tariff.class)
              .transform(resultSet));
        }
      }
    }
    return tariffs;
  }

  @Override
  public Tariff getByKey(Integer key) throws SQLException {
    Tariff entity = null;
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_KEY)) {
      ps.setInt(1, key);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity = (Tariff) new Transformer(Tariff.class)
              .transform(resultSet);
          break;
        }
      }
    }
    return entity;
  }

  @Override
  public int create(Tariff entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getId());
      ps.setString(2, entity.getName());
      ps.setInt(3, entity.getFee());
      ps.setInt(4, entity.getOperatorId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(Tariff entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
      ps.setString(1, entity.getName());
      ps.setInt(2, entity.getFee());
      ps.setInt(3, entity.getOperatorId());
      ps.setInt(4, entity.getId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(Integer key) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
      ps.setInt(1, key);
      return ps.executeUpdate();
    }
  }
}
