package com.epam.task18.jdbc.dao.implementation;

import com.epam.task18.jdbc.dao.interfaces.ClientDao;
import com.epam.task18.jdbc.model.Client;
import com.epam.task18.jdbc.utilities.JdbcConnector;
import com.epam.task18.jdbc.utilities.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDaoImp implements ClientDao {

  private static final String GET_ALL = "SELECT * FROM client";
  private static final String GET_BY_KEY = "SELECT * FROM client WHERE id=?";
  private static final String CREATE = "INSERT client (id, number, name, tariff_id) VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE client SET number=?, name=?, tariff_id=? WHERE id=?";
  private static final String DELETE = "DELETE FROM client WHERE id=?";
  private static final String GET_BY_OPERATOR = "SELECT *\n"
      + "FROM client c\n"
      + "INNER JOIN tariff t ON c.tariff_id = t.id\n"
      + "INNER JOIN mobile_operator m ON t.operator_id = m.id\n"
      + "WHERE m.id = ?";

  @Override
  public List<Client> getAll() throws SQLException {
    List<Client> clients = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          clients.add((Client) new Transformer(Client.class)
              .transform(resultSet));
        }
      }
    }
    return clients;
  }

  @Override
  public Client getByKey(Integer key) throws SQLException {
    Client entity = null;
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_KEY)) {
      ps.setInt(1, key);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity = (Client) new Transformer(Client.class)
              .transform(resultSet);
          break;
        }
      }
    }
    return entity;
  }

  @Override
  public int create(Client entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getId());
      ps.setString(2, entity.getNumber());
      ps.setString(3, entity.getName());
      ps.setInt(4, entity.getTariffId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(Client entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
      ps.setString(1, entity.getNumber());
      ps.setString(2, entity.getName());
      ps.setInt(3, entity.getTariffId());
      ps.setInt(4, entity.getId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(Integer key) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
      ps.setInt(1, key);
      return ps.executeUpdate();
    }
  }

  @Override
  public List<Client> getByMobileOperator(Integer mobileOperatorId) throws SQLException {
    List<Client> clients = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_OPERATOR)) {
      ps.setInt(1, mobileOperatorId);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          clients.add((Client) new Transformer(Client.class)
              .transform(resultSet));
        }
      }
    }
    return clients;
  }
}
