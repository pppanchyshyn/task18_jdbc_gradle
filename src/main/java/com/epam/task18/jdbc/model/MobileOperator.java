package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.Column;
import com.epam.task18.jdbc.utilities.annotations.PrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;

@Table(name = "mobile_operator")
public class MobileOperator {

  @PrimaryKey()
  @Column(name = "id")
  private Integer id;
  @Column(name = "name")
  private String name;

  public MobileOperator() {
  }

  public MobileOperator(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return String.format("%-11d%-20s", id, name);
  }
}
