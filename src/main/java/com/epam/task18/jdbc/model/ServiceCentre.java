package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.Column;
import com.epam.task18.jdbc.utilities.annotations.PrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;

@Table(name = "service_centre")
public class ServiceCentre {

  @PrimaryKey()
  @Column(name = "id")
  private Integer id;
  @Column(name = "city")
  private String city;
  @Column(name = "address")
  private String address;
  @Column(name = "operator_id")
  private Integer operatorId;

  public ServiceCentre() {
  }

  public ServiceCentre(Integer id, String city, String address, Integer operatorId) {
    this.id = id;
    this.city = city;
    this.address = address;
    this.operatorId = operatorId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public Integer getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(Integer operatorId) {
    this.operatorId = operatorId;
  }

  @Override
  public String toString() {
    return String.format("%-11d%-20s%-20s%-11d", id, city, address, operatorId);
  }
}
