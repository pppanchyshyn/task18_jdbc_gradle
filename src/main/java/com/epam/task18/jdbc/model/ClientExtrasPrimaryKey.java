package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.Column;

public class ClientExtrasPrimaryKey {

  @Column(name = "client_id")
  private Integer clientId;
  @Column(name = "extras_id")
  private Integer extrasId;

  public ClientExtrasPrimaryKey() {
  }

  public ClientExtrasPrimaryKey(Integer clientId, Integer extrasId) {
    this.clientId = clientId;
    this.extrasId = extrasId;
  }

  public Integer getClientId() {
    return clientId;
  }

  public void setClientId(Integer clientId) {
    this.clientId = clientId;
  }

  public Integer getExtrasId() {
    return extrasId;
  }

  public void setExtrasId(Integer extrasId) {
    this.extrasId = extrasId;
  }

  @Override
  public String toString() {
    return String.format("%-11d%-11d", clientId, extrasId);
  }
}
