package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.Column;
import com.epam.task18.jdbc.utilities.annotations.PrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;
import java.sql.Date;


@Table(name = "account_info")
public class AccountInfo {

  @PrimaryKey()
  @Column(name = "id")
  private Integer id;
  @Column(name = "client_id")
  private Integer clientId;
  @Column(name = "balance")
  private Double balance;
  @Column(name = "number_validity")
  private Date numberValidity;

  public AccountInfo() {
  }

  public AccountInfo(Integer id, Integer clientId, Double balance, Date numberValidity) {
    this.id = id;
    this.clientId = clientId;
    this.balance = balance;
    this.numberValidity = numberValidity;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getClientId() {
    return clientId;
  }

  public void setClientId(Integer clientId) {
    this.clientId = clientId;
  }

  public Double getBalance() {
    return balance;
  }

  public void setBalance(Double balance) {
    this.balance = balance;
  }

  public Date getNumberValidity() {
    return numberValidity;
  }

  public void setNumberValidity(Date numberValidity) {
    this.numberValidity = numberValidity;
  }

  @Override
  public String toString() {
    return String.format("%-11d%-11d%-11.2f%-20s", id, clientId, balance, numberValidity.toString());
  }
}
