package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.Column;
import com.epam.task18.jdbc.utilities.annotations.PrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;

@Table(name = "extras")
public class Extras {

  @PrimaryKey()
  @Column(name = "id")
  private Integer id;
  @Column(name = "name")
  private String name;
  @Column(name = "description")
  private String description;
  @Column(name = "price")
  private Integer price;
  @Column(name = "operator_id")
  private Integer operatorId;

  public Extras() {
  }

  public Extras(Integer id, String name, String description, Integer price,
      Integer operatorId) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.operatorId = operatorId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(Integer operatorId) {
    this.operatorId = operatorId;
  }

  @Override
  public String toString() {
    return String.format("%-11d%-25s%-75s%-11d%-11d", id, name, description, price, operatorId);
  }
}
