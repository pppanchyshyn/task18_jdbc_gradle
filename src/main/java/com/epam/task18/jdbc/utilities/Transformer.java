package com.epam.task18.jdbc.utilities;

import com.epam.task18.jdbc.utilities.annotations.Column;
import com.epam.task18.jdbc.utilities.annotations.CompositePrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class Transformer<T> {

  private final Class<T> clazz;

  public Transformer(Class<T> clazz) {
    this.clazz = clazz;
  }

  public Object transform(ResultSet rs)
      throws SQLException {
    Object entity = null;
    try {
      entity = Objects.requireNonNull(clazz.getConstructor()).newInstance();
      if (clazz.isAnnotationPresent(Table.class)) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
          if (field.isAnnotationPresent(Column.class)) {
            Column column = field.getAnnotation(Column.class);
            String name = column.name();
            field.setAccessible(true);
            Class fieldType = field.getType();
            if (fieldType == String.class) {
              field.set(entity, rs.getString(name));
            } else if (fieldType == Integer.class) {
              field.set(entity, rs.getInt(name));
            } else if (fieldType == Date.class) {
              field.set(entity, rs.getDate(name));
            } else if (fieldType == Double.class) {
              field.set(entity, rs.getDouble(name));
            }
          } else if (field.isAnnotationPresent(CompositePrimaryKey.class)) {
            field.setAccessible(true);
            Class fieldType = field.getType();
            Object FK = Objects.requireNonNull(fieldType.getConstructor()).newInstance();
            field.set(entity, FK);
            Field[] fieldsInner = fieldType.getDeclaredFields();
            for (Field fieldInner : fieldsInner) {
              if (fieldInner.isAnnotationPresent(Column.class)) {
                Column column = fieldInner.getAnnotation(Column.class);
                String name = column.name();
                fieldInner.setAccessible(true);
                Class fieldInnerType = fieldInner.getType();
                if (fieldInnerType == String.class) {
                  fieldInner.set(FK, rs.getString(name));
                } else if (fieldInnerType == Integer.class) {
                  fieldInner.set(FK, rs.getInt(name));
                }
              }
            }
          }
        }
      }
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
      e.printStackTrace();
    }
    return entity;
  }
}
