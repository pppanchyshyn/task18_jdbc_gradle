package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.business.logic.ClientBL;
import com.epam.task18.jdbc.model.Client;
import java.util.List;

public class ClientController extends AbstractController {

  void getClientTable() {
    logger.info("\nClients:");
    ClientBL bl = new ClientBL();
    List<Client> clients = bl.getAll();
    for (Client client : clients) {
      logger.info(client);
    }
  }

  void getClientById() {
    logger.info("Enter Client id: ");
    Integer id = enterInteger();
    input.nextLine();
    ClientBL bl = new ClientBL();
    Client client = bl.getByKey(id);
    logger.info(client);
  }

  void getClientByMobileOperator() {
    logger.info("Enter Mobile operator id: ");
    Integer id = enterInteger();
    input.nextLine();
    ClientBL bl = new ClientBL();
    List<Client> clients = bl.getByMobileOperator(id);
    for (Client client : clients) {
      logger.info(client);
    }
  }

  void createNewClient() {
    logger.info("Input Client id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Client number: ");
    String number = input.nextLine();
    logger.info("Input Client name: ");
    String name = input.nextLine();
    logger.info("Input tariff_id: ");
    Integer tariffId = enterInteger();
    Client client = new Client(id, number, name, tariffId);
    ClientBL bl = new ClientBL();
    int count = bl.create(client);
    logger.info(String.format("%d row created", count));
  }

  void updateClient() {
    logger.info("Input Client id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Client number: ");
    String number = input.nextLine();
    logger.info("Input Client name: ");
    String name = input.nextLine();
    logger.info("Input tariff_id: ");
    Integer tariffId = enterInteger();
    Client client = new Client(id, number, name, tariffId);
    ClientBL bl = new ClientBL();
    int count = bl.update(client);
    logger.info(String.format("%d row updated", count));
  }

  void deleteClientByID() {
    logger.info("Input Client id: ");
    Integer id = enterInteger();
    input.nextLine();
    ClientBL bl = new ClientBL();
    int count = bl.delete(id);
    logger.info(String.format("%d row was deleted", count));
  }
}
