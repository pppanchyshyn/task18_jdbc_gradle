package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.business.logic.ExtrasBL;
import com.epam.task18.jdbc.model.Extras;
import java.util.List;

public class ExtrasController extends AbstractController {

  void getExtrasTable() {
    logger.info("\nExtras:");
    ExtrasBL bl = new ExtrasBL();
    List<Extras> extrasList = bl.getAll();
    for (Extras extras : extrasList) {
      logger.info(extras);
    }
  }

  void getExtrasById() {
    logger.info("Enter id of Extra: ");
    Integer id = enterInteger();
    input.nextLine();
    ExtrasBL bl = new ExtrasBL();
    Extras extras = bl.getByKey(id);
    logger.info(extras);
  }

  void createNewExtra() {
    logger.info("Input Extra id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Extra name: ");
    String name = input.nextLine();
    logger.info("Input Extra description: ");
    String description = input.nextLine();
    logger.info("Input Extra price: ");
    Integer price = enterInteger();
    input.nextLine();
    logger.info("Input operator_id for Extra: ");
    Integer operatorId = enterInteger();
    input.nextLine();
    Extras extras = new Extras(id, name, description, price, operatorId);
    ExtrasBL bl = new ExtrasBL();
    int count = bl.create(extras);
    logger.info(String.format("%d row created", count));
  }

  void updateExtra() {
    logger.info("Input Extra id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Extra name: ");
    String name = input.nextLine();
    logger.info("Input Extra description: ");
    String description = input.nextLine();
    logger.info("Input Extra price: ");
    Integer price = enterInteger();
    input.nextLine();
    logger.info("Input operator_id for Extra: ");
    Integer operatorId = enterInteger();
    input.nextLine();
    Extras extras = new Extras(id, name, description, price, operatorId);
    ExtrasBL bl = new ExtrasBL();
    int count = bl.update(extras);
    logger.info(String.format("%d row updated", count));
  }

  void deleteExtrasByID() {
    logger.info("Input Extra id:: ");
    Integer id = enterInteger();
    input.nextLine();
    ExtrasBL bl = new ExtrasBL();
    int count = bl.delete(id);
    logger.info(String.format("%d row was deleted", count));
  }
}
