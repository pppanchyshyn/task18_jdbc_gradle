package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.business.logic.TariffTermsBL;
import com.epam.task18.jdbc.model.TariffTerms;
import java.util.List;

public class TariffTermsController extends AbstractController {

  void getTariffTermsTable() {
    logger.info("\nTariff Terms:");
    TariffTermsBL bl = new TariffTermsBL();
    List<TariffTerms> tariffTermsList = bl.getAll();
    for (TariffTerms tariffTerm : tariffTermsList) {
      logger.info(tariffTerm);
    }
  }

  void getTariffTermById() {
    logger.info("Enter tariff_id of Tariff Terms: ");
    Integer id = enterInteger();
    input.nextLine();
    TariffTermsBL bl = new TariffTermsBL();
    TariffTerms tariffTerm = bl.getByKey(id);
    logger.info(tariffTerm);
  }

  void createNewTariffTerm() {
    logger.info("Input Tariff term id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input tariff_id for Tariff term: ");
    Integer tariffId = enterInteger();
    input.nextLine();
    logger.info("Input minutes for Tariff term: ");
    Integer minutes = enterInteger();
    input.nextLine();
    logger.info("Input sms for Tariff term: ");
    Integer sms = enterInteger();
    input.nextLine();
    logger.info("Input internet for Tariff term: ");
    Integer internet = enterInteger();
    input.nextLine();
    TariffTerms tariffTerm = new TariffTerms(id, tariffId, minutes, sms, internet);
    TariffTermsBL bl = new TariffTermsBL();
    int count = bl.create(tariffTerm);
    logger.info(String.format("%d row created", count));
  }

  void updateTariffTerm() {
    logger.info("Input Tariff term id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input tariff_id for Tariff term: ");
    Integer tariffId = enterInteger();
    input.nextLine();
    logger.info("Input minutes for Tariff term: ");
    Integer minutes = enterInteger();
    input.nextLine();
    logger.info("Input sms for Tariff term: ");
    Integer sms = enterInteger();
    input.nextLine();
    logger.info("Input internet for Tariff term: ");
    Integer internet = enterInteger();
    input.nextLine();
    TariffTerms tariffTerm = new TariffTerms(id, tariffId, minutes, sms, internet);
    TariffTermsBL bl = new TariffTermsBL();
    int count = bl.update(tariffTerm);
    logger.info(String.format("%d row updated", count));
  }

  void deleteTariffTermByID() {
    logger.info("Input tariff_id for Tariff term: ");
    Integer id = enterInteger();
    input.nextLine();
    TariffTermsBL bl = new TariffTermsBL();
    int count = bl.delete(id);
    logger.info(String.format("%d row was deleted", count));
  }
}
