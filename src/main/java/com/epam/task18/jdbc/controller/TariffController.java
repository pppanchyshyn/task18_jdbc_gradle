package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.business.logic.TariffBL;
import com.epam.task18.jdbc.model.Tariff;
import java.util.List;

public class TariffController extends AbstractController {

  void getTariffTable() {
    logger.info("\nTariff:");
    TariffBL bl = new TariffBL();
    List<Tariff> tariffs = bl.getAll();
    for (Tariff tariff : tariffs) {
      logger.info(tariff);
    }
  }

  void getTariffById() {
    logger.info("Enter id of Tariff: ");
    Integer id = enterInteger();
    input.nextLine();
    TariffBL bl = new TariffBL();
    Tariff tariff = bl.getByKey(id);
    logger.info(tariff);
  }

  void createNewTariff() {
    logger.info("Input Tariff id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Tariff name: ");
    String name = input.nextLine();
    logger.info("Input Tariff fee: ");
    Integer fee = enterInteger();
    input.nextLine();
    logger.info("Input operator_id for Tariff: ");
    Integer operatorId = enterInteger();
    input.nextLine();
    Tariff tariff = new Tariff(id, name, fee, operatorId);
    TariffBL bl = new TariffBL();
    int count = bl.create(tariff);
    logger.info(String.format("%d row created", count));
  }

  void updateTariff() {
    logger.info("Input Tariff id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Tariff name: ");
    String name = input.nextLine();
    logger.info("Input Tariff fee: ");
    Integer fee = enterInteger();
    input.nextLine();
    logger.info("Input operator_id for Tariff: ");
    Integer operatorId = enterInteger();
    input.nextLine();
    Tariff tariff = new Tariff(id, name, fee, operatorId);
    TariffBL bl = new TariffBL();
    int count = bl.update(tariff);
    logger.info(String.format("%d row updated", count));
  }

  void deleteTariffByID() {
    logger.info("Input Tariff id: ");
    Integer id = enterInteger();
    input.nextLine();
    TariffBL bl = new TariffBL();
    int count = bl.delete(id);
    logger.info(String.format("%d row was deleted", count));
  }
}
