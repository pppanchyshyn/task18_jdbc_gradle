package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.view.Printable;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {

  private Logger logger = LogManager.getLogger(Controller.class);
  private Map<String, Printable> functionalMenu;
  private MobileOperatorController mobileOperatorController;
  private TariffController tariffController;
  private ServiceCentreController serviceCentreController;
  private ExtrasController extrasController;
  private TariffTermsController tariffTermsController;
  private ClientController clientController;
  private AccountInfoController accountInfoController;
  private ClientExtrasController clientExtrasController;

  public Controller() {
    mobileOperatorController = new MobileOperatorController();
    tariffController = new TariffController();
    serviceCentreController = new ServiceCentreController();
    extrasController = new ExtrasController();
    tariffTermsController = new TariffTermsController();
    clientController = new ClientController();
    accountInfoController = new AccountInfoController();
    clientExtrasController = new ClientExtrasController();
    functionalMenu = new LinkedHashMap<>();
    functionalMenu.put("A", this::getAllTables);
    functionalMenu.put("11", mobileOperatorController::getOperatorTable);
    functionalMenu.put("12", mobileOperatorController::getOperatorById);
    functionalMenu.put("13", mobileOperatorController::createNewOperator);
    functionalMenu.put("14", mobileOperatorController::updateOperator);
    functionalMenu.put("15", mobileOperatorController::deleteOperatorByID);
    functionalMenu.put("21", tariffController::getTariffTable);
    functionalMenu.put("22", tariffController::getTariffById);
    functionalMenu.put("23", tariffController::createNewTariff);
    functionalMenu.put("24", tariffController::updateTariff);
    functionalMenu.put("25", tariffController::deleteTariffByID);
    functionalMenu.put("31", serviceCentreController::getServiceCentreTable);
    functionalMenu.put("32", serviceCentreController::getServiceCentreById);
    functionalMenu.put("33", serviceCentreController::createNewServiceCentre);
    functionalMenu.put("34", serviceCentreController::updateServiceCentre);
    functionalMenu.put("35", serviceCentreController::deleteServiceCentreByID);
    functionalMenu.put("41", extrasController::getExtrasTable);
    functionalMenu.put("42", extrasController::getExtrasById);
    functionalMenu.put("43", extrasController::createNewExtra);
    functionalMenu.put("44", extrasController::updateExtra);
    functionalMenu.put("45", extrasController::deleteExtrasByID);
    functionalMenu.put("51", tariffTermsController::getTariffTermsTable);
    functionalMenu.put("52", tariffTermsController::getTariffTermById);
    functionalMenu.put("53", tariffTermsController::createNewTariffTerm);
    functionalMenu.put("54", tariffTermsController::updateTariffTerm);
    functionalMenu.put("55", tariffTermsController::deleteTariffTermByID);
    functionalMenu.put("61", clientController::getClientTable);
    functionalMenu.put("62", clientController::getClientById);
    functionalMenu.put("63", clientController::getClientByMobileOperator);
    functionalMenu.put("64", clientController::createNewClient);
    functionalMenu.put("65", clientController::updateClient);
    functionalMenu.put("66", clientController::deleteClientByID);
    functionalMenu.put("71", accountInfoController::getAccountInfoTable);
    functionalMenu.put("72", accountInfoController::getAccountInfoById);
    functionalMenu.put("73", accountInfoController::createNewAccountInfo);
    functionalMenu.put("74", accountInfoController::updateAccountInfo);
    functionalMenu.put("75", accountInfoController::deleteAccountInfoByID);
    functionalMenu.put("81", clientExtrasController::getClientExtrasTable);
    functionalMenu.put("82", clientExtrasController::getClientExtrasByClient);
    functionalMenu.put("83", clientExtrasController::getClientExtrasByExtas);
    functionalMenu.put("84", clientExtrasController::createClientExtras);
    functionalMenu.put("85", clientExtrasController::deleteClientExtras);
    functionalMenu.put("E", this::goodLuck);
  }

  private void getAllTables() {
    mobileOperatorController.getOperatorTable();
    tariffController.getTariffTable();
    serviceCentreController.getServiceCentreTable();
    extrasController.getExtrasTable();
    tariffTermsController.getTariffTermsTable();
    clientController.getClientTable();
    accountInfoController.getAccountInfoTable();
    clientExtrasController.getClientExtrasTable();
  }

  private void goodLuck() {
    logger.info("Good luck");
  }

  public Map<String, Printable> getFunctionalMenu() {
    return functionalMenu;
  }
}
