package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.business.logic.ServiceCentreBL;
import com.epam.task18.jdbc.model.ServiceCentre;
import java.util.List;

public class ServiceCentreController extends AbstractController {

  void getServiceCentreTable() {
    logger.info("\nService Centre:");
    ServiceCentreBL bl = new ServiceCentreBL();
    List<ServiceCentre> centres = bl.getAll();
    for (ServiceCentre centre : centres) {
      logger.info(centre);
    }
  }

  void getServiceCentreById() {
    logger.info("Enter id of Service Centre: ");
    Integer id = enterInteger();
    input.nextLine();
    ServiceCentreBL bl = new ServiceCentreBL();
    ServiceCentre centre = bl.getByKey(id);
    logger.info(centre);
  }

  void createNewServiceCentre() {
    logger.info("Input Service Centre id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Service Centre city: ");
    String city = input.nextLine();
    logger.info("Input Service Centre address: ");
    String address = input.nextLine();
    logger.info("Input operator_id for Service Centre: ");
    Integer operatorId = enterInteger();
    input.nextLine();
    ServiceCentre serviceCentre = new ServiceCentre(id, city, address, operatorId);
    ServiceCentreBL bl = new ServiceCentreBL();
    int count = bl.create(serviceCentre);
    logger.info(String.format("%d row created", count));
  }

  void updateServiceCentre() {
    logger.info("Input Service Centre id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Service Centre city: ");
    String city = input.nextLine();
    logger.info("Input Service Centre address: ");
    String address = input.nextLine();
    logger.info("Input operator_id for Service Centre: ");
    Integer operatorId = enterInteger();
    input.nextLine();
    ServiceCentre serviceCentre = new ServiceCentre(id, city, address, operatorId);
    ServiceCentreBL bl = new ServiceCentreBL();
    int count = bl.create(serviceCentre);
    logger.info(String.format("%d row updated", count));
  }

  void deleteServiceCentreByID() {
    logger.info("Input Service Centre id: ");
    Integer id = enterInteger();
    input.nextLine();
    ServiceCentreBL bl = new ServiceCentreBL();
    int count = bl.delete(id);
    logger.info(String.format("%d row was deleted", count));
  }
}
