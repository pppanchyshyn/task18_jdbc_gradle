package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.business.logic.AccountInfoBL;
import com.epam.task18.jdbc.model.AccountInfo;
import java.sql.Date;
import java.util.List;

public class AccountInfoController extends AbstractController {

  void getAccountInfoTable() {
    logger.info("\nAccount info:");
    AccountInfoBL bl = new AccountInfoBL();
    List<AccountInfo> accountInfos = bl.getAll();
    for (AccountInfo accountInfo : accountInfos) {
      logger.info(accountInfo);
    }
  }

  void getAccountInfoById() {
    logger.info("Enter client_id for Account info: ");
    Integer id = enterInteger();
    input.nextLine();
    AccountInfoBL bl = new AccountInfoBL();
    AccountInfo accountInfo = bl.getByKey(id);
    logger.info(accountInfo);
  }

  void createNewAccountInfo() {
    logger.info("Input Account info id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input client_id for Account info: ");
    Integer clientId = enterInteger();
    input.nextLine();
    logger.info("Input Account info balance: ");
    Double balance = enterDouble();
    input.nextLine();
    logger.info("Input number_validity for Account info (2000-01-01): ");
    String numberValidity = input.nextLine();
    Date date = Date.valueOf(numberValidity);
    AccountInfo accountInfo = new AccountInfo(id, clientId, balance, date);
    AccountInfoBL bl = new AccountInfoBL();
    int count = bl.create(accountInfo);
    logger.info(String.format("%d row created", count));
  }

  void updateAccountInfo() {
    logger.info("Input Account info id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input client_id for Account info: ");
    Integer clientId = enterInteger();
    input.nextLine();
    logger.info("Input Account info balance: ");
    Double balance = enterDouble();
    input.nextLine();
    logger.info("Input number_validity for Account info (2000-12-24): ");
    String numberValidity = input.nextLine();
    Date date = Date.valueOf(numberValidity);
    AccountInfo accountInfo = new AccountInfo(id, clientId, balance, date);
    AccountInfoBL bl = new AccountInfoBL();
    int count = bl.update(accountInfo);
    logger.info(String.format("%d row updated", count));
  }

  void deleteAccountInfoByID() {
    logger.info("Input client_id for Account info: ");
    Integer id = enterInteger();
    input.nextLine();
    AccountInfoBL bl = new AccountInfoBL();
    int count = bl.delete(id);
    logger.info(String.format("%d row was deleted", count));
  }
}
