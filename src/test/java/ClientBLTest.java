import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.epam.task18.jdbc.business.logic.AccountInfoBL;
import com.epam.task18.jdbc.business.logic.ClientBL;
import com.epam.task18.jdbc.model.Client;
import com.epam.task18.jdbc.view.ConsoleView;
import org.junit.jupiter.api.Test;

class ClientBLTest extends AbstractTest {

  private ClientBL clientBL = new ClientBL();
  private AccountInfoBL accountInfoBL = new AccountInfoBL();

  @Test
  void testGetByKeyPositive() {
    ConsoleView.logger.info("Getting client by id is testing...");
    int id = 1;
    String realClientName = "Oleh";
    assertEquals(realClientName, clientBL.getByKey(id).getName(),
        "Can't client operator by id");
  }

  @Test
  void testCreatePositive() {
    ConsoleView.logger.info("Creating new client is testing...");
    Client testedClient = new Client(40, "380965555568", "Ira", 1);
    clientBL.create(testedClient);
    assertNotNull(clientBL.getByKey(testedClient.getId()), "client isn't created");
    assertEquals(testedClient.getNumber(), clientBL.getByKey(testedClient.getId()).getNumber(),
        "client was created wrong");
  }

  @Test
  void testCreateNegative() {
    ConsoleView.logger.info("Fail of creating client with invalid data is testing...");
    Client testedClient = new Client(40, "380961024568", "Ira", 1);
    assertNotEquals(successfulOperationValue, clientBL.create(testedClient),
        "Client with invalid data was created");
  }

  @Test
  void testDelete() {
    ConsoleView.logger.info("Client deleting is testing...");
    int deletedId = 1;
    clientBL.delete(deletedId);
    assertNull(clientBL.getByKey(deletedId), "Client wasn't deleted");
    assertNull(accountInfoBL.getByKey(deletedId), "Data from related tables has not been deleted");
  }
}
