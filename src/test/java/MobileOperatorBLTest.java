import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.epam.task18.jdbc.business.logic.MobileOperatorBL;
import com.epam.task18.jdbc.business.logic.ServiceCentreBL;
import com.epam.task18.jdbc.model.MobileOperator;
import com.epam.task18.jdbc.view.ConsoleView;
import org.junit.jupiter.api.*;


class MobileOperatorBLTest extends AbstractTest {

  private MobileOperatorBL mobileOperatorBL = new MobileOperatorBL();
  private ServiceCentreBL serviceCentreBL = new ServiceCentreBL();

  @Test
  void testGetByKey() {
    ConsoleView.logger.info("Getting mobile operator by id is testing...");
    MobileOperator testedMO = new MobileOperator(1, "Kyivstar");
    assertEquals(testedMO.getName(), mobileOperatorBL.getByKey(testedMO.getId()).getName(),
        "Can't find mobile operator by id");
  }

  @Test
  void testCreatePositive() {
    ConsoleView.logger.info("Creating new mobile operator is testing...");
    MobileOperator testedMO = new MobileOperator(4, "T-Mobile");
    mobileOperatorBL.create(testedMO);
    assertNotNull(mobileOperatorBL.getByKey(testedMO.getId()), "mobile operator isn't created");
    assertEquals(testedMO.getName(), mobileOperatorBL.getByKey(testedMO.getId()).getName(),
        "mobile operator was created wrong");
  }

  @Test
  void testUpdate() {
    ConsoleView.logger.info("Mobile operator updating is testing...");
    MobileOperator testedMO = new MobileOperator(3, "T-Mobile");
    MobileOperator oldMobileOperator = mobileOperatorBL.getByKey(testedMO.getId());
    mobileOperatorBL.update(testedMO);
    assertNotEquals(testedMO, oldMobileOperator, "Mobile operator wasn't updated");
  }

  @Test
  void testDeletePositive() {
    ConsoleView.logger.info("Mobile operator deleting is testing...");
    int deletedId = 3;
    int serviceCentreDeletedId = 3;
    mobileOperatorBL.delete(deletedId);
    assertNull(mobileOperatorBL.getByKey(deletedId), "Mobile operator wasn't deleted");
    assertNull(serviceCentreBL.getByKey(serviceCentreDeletedId),
        "Data from related tables has not been deleted");
  }
}
