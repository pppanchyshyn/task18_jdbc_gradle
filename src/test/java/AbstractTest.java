import com.epam.task18.jdbc.utilities.JdbcConnector;
import java.sql.Connection;
import java.sql.SQLException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

abstract class AbstractTest {

  private static Connection connection;
  final int successfulOperationValue = 1;

  @BeforeEach
  void setUp() {
    connection = JdbcConnector.getTestConnection();
    try {
      connection.setAutoCommit(false);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @AfterEach
  void tirDown() {
    try {
      connection.rollback();
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
