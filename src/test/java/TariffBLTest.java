import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.epam.task18.jdbc.business.logic.ClientBL;
import com.epam.task18.jdbc.business.logic.TariffBL;
import com.epam.task18.jdbc.business.logic.TariffTermsBL;
import com.epam.task18.jdbc.model.Tariff;
import com.epam.task18.jdbc.view.ConsoleView;
import org.junit.jupiter.api.Test;

class TariffBLTest extends AbstractTest {

  private TariffBL tariffBL = new TariffBL();
  private TariffTermsBL tariffTermsBL = new TariffTermsBL();
  private ClientBL clientBL = new ClientBL();

  @Test
  void testGetByKey() {
    ConsoleView.logger.info("Getting tariff by id is testing...");
    int id = 1;
    String tariffName = "Light";
    assertEquals(tariffName, tariffBL.getByKey(id).getName(), "Can't find tariff by id");
  }

  @Test
  void testCreatePositive() {
    ConsoleView.logger.info("Creating new tariff is testing...");
    Tariff testedTariff = new Tariff(15, "New Tariff", 200, 1);
    tariffBL.create(testedTariff);
    assertNotNull(tariffBL.getByKey(testedTariff.getId()), "tariff isn't created");
    assertEquals(testedTariff.getName(), tariffBL.getByKey(testedTariff.getId()).getName(),
        "tariff was created wrong");
  }

  @Test
  void testCreateNegative() {
    ConsoleView.logger.info("Fail of creating tariff with invalid data is testing...");
    Tariff testedTariff = new Tariff(45, "New Tariff", 200, 5);
    assertNotEquals(successfulOperationValue, tariffBL.create(testedTariff),
        "tariff with invalid data was created");
  }

  @Test
  void testDeletePositive() {
    ConsoleView.logger.info("Tariff deleting is testing...");
    int deletedId = 1;
    int clientId = 1;
    tariffBL.delete(deletedId);
    assertNull(tariffBL.getByKey(deletedId), "tariff wasn't deleted");
    assertNull(tariffTermsBL.getByKey(deletedId),
        "Related data from tariff_terms table has not been deleted");
    assertNull(clientBL.getByKey(clientId), "Related data from client table has not been deleted");
  }
}
