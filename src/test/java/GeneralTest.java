import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectClasses({MobileOperatorBLTest.class, ClientBLTest.class, TariffBLTest.class,
    ExtrasBLTest.class})
public class GeneralTest {
}
